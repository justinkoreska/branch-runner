import fetch from "node-fetch";

class BambooService {

  constructor(apiBase, planKey) {
    this._apiBase = apiBase;
    this._planKey = planKey;
  }

  fetchBranches() {
    const url =
      `${this._apiBase}/rest/api/latest/plan/${this._planKey}/branch.json?max-result=9999`;
    return fetch(url)
      .then(response => response.json())
      .then(json => {
        const { branches: { branch = [] } = {} } = json;
        return branch
          .filter(branch => /^issue-/.test(branch.shortName))
          .map(branch => ({
            key: branch.key,
            name: normalizeBranchName(branch.shortName),
          }))
          .concat([{
            key: this._planKey,
            name: "master",
          }]);
      });
  }

  fetchResult(branchKey) {
    const url =
      `${this._apiBase}/rest/api/latest/result/${branchKey}.json?expand=results.result.artifacts`;
    return fetch(url)
      .then(response => response.json())
      .then(json => {
        const { results: { result: results = [] } = {} } = json;
        const result = results.length && results
          .filter(result => "Successful" == result.buildState)
          .sort((a, b) => b.buildNumber - a.buildNumber)
          [0] || {};
        const { artifacts: { artifact = [] } = {} } = result;
        return {
          key: branchKey,
          name: normalizeBranchName(result.planName),
          build: result.buildNumber,
          commit: result.vcsRevisionKey,
          summary: result.buildReason,
          artifacts: artifact.map(artifact => ({
            name: artifact.name.replace(/\-jar$/, "-site").trim(),
            href: artifact.link.href,
            size: artifact.size,
          })),
        };
      });
  }

}

const normalizeBranchName = name =>
  name
    .replace(/[_-]/g, " ")
    .replace(/^issue fe /i, "FE-");

export default BambooService;
