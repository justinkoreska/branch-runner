
import types from "./types";

const load = () => (dispatch, getState, services) => {
  dispatch({
    type: types.LOAD,
  });

  const branchService = services.branchService;

  Promise.all([
    branchService.fetchBranches(),
    branchService.getRuningBranchLimit(),
  ]).then(([branchResult, branchLimitResult]) => dispatch({
    type: types.LOADED,
    payload: branchResult,
    branchLimit: branchLimitResult.branchLimit,
  }));
};

const select = key => ({
  type: types.SELECT,
  payload: key,
});

const initialize = key => (dispatch, getState, services) => {
  dispatch({
    type: types.INITIALIZE,
    payload: key,
  });
  return services.branchService
    .initializeBranch(key)
    .then(branch => dispatch({
      type: types.INITIALIZED,
      payload: branch,
    }));
};

const remove = key => (dispatch, getState, services) => {
  dispatch({
    type: types.REMOVE,
    payload: key,
  });
  return services.branchService
    .removeBranch(key)
    .then(branch => dispatch({
      type: types.REMOVED,
      payload: key,
    }));
};

const run = (key, artifactName) => (dispatch, getState, services) => {
  dispatch({
    type: types.RUN,
    payload: {
      key,
      artifactName,
    },
  });
  return services.branchService
    .runBranch(key, artifactName)
    .then(branch => dispatch({
      type: types.RUNNING,
      payload: branch,
    }));
};

const stop = (key, artifactName) => (dispatch, getState, services) => {
  dispatch({
    type: types.STOP,
    payload: {
      key,
      artifactName,
    },
  });
  return services.branchService
    .stopBranch(key, artifactName)
    .then(branch => dispatch({
      type: types.STOPPED,
      payload: branch,
    }));
};

const addSetting = (key, settingKey, settingValue) => ({
  type: types.ADD_SETTING,
  payload: {
    key,
    settingKey,
    settingValue,
  },
});

const removeSetting = (key, settingKey) => ({
  type: types.REMOVE_SETTING,
  payload: {
    key,
    settingKey,
  },
});

const save = branch => (dispatch, getState, services) => {
  dispatch({
    type: types.SAVE,
    payload: {
      key: branch.key,
    },
  });
  return services.branchService
    .saveBranch(branch)
    .then(branch => dispatch({
      type: types.SAVED,
      payload: branch,
    }));
};

export default {
  load,
  select,
  initialize,
  remove,
  run,
  stop,
  addSetting,
  removeSetting,
  save,
};
