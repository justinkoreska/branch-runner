import types from "./types";

const defaultState = {
  all: [],
  branchLimit: 0,
};

const load = (state, action) =>
  Object.assign({}, state, {
    isLoading: true,
  });

const loaded = (state, action) =>
  Object.assign({}, state, {
    isLoading: false,
    all: action.payload,
    branchLimit: action.branchLimit,
  });

const select = (state, action) =>
  Object.assign({}, state, {
    selected: action.payload,
  });

const initialize = (state, action) => {
  const all = state.all.map(branch =>
    action.payload == branch.key
      ? { ...branch, isInitializing: true }
      : branch
  );
  return Object.assign({}, state, { all });
};

const initialized = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? action.payload
      : branch
  );
  return Object.assign({}, state, { all });
};

const remove = (state, action) => {
  const all = state.all.map(branch =>
    action.payload == branch.key
      ? { ...branch, isRemoving: true }
      : branch
  );
  return Object.assign({}, state, { all });
};

const removed = (state, action) => {
  const all = state.all.map(branch =>
    action.payload == branch.key
      ? { key: branch.key, name: branch.name }
      : branch
  );
  return Object.assign({}, state, { all });
};

const wait = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? {
        ...branch,
        artifacts: branch.artifacts.map(artifact => (
          action.payload.artifactName == artifact.name
            ? {
              ...artifact,
              isWaiting: true,
            }
            : artifact
        )),
      }
      : branch
  );
  return Object.assign({}, state, { all });
};

const running = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? action.payload
      : branch
  );
  return Object.assign({}, state, { all });
};

const stopped = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? action.payload
      : branch
  );
  return Object.assign({}, state, { all });
};

const addSetting = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? {
          ...branch,
          settings: {
            ...branch.settings,
            [action.payload.settingKey]: action.payload.settingValue,
          },
        }
      : branch
  );
  return Object.assign({}, state, { all });
};

const removeSetting = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? {
          ...branch,
          settings: Object.keys(branch.settings)
            .reduce((settings, key) => {
              if (key == action.payload.settingKey)
                delete settings[key];
              return settings;
            }, branch.settings),
        }
      : branch
  );
  return Object.assign({}, state, { all });
};

const save = (state, action) => {
  return wait(state, action);
};

const saved = (state, action) => {
  const all = state.all.map(branch =>
    action.payload.key == branch.key
      ? action.payload
      : branch
  );
  return Object.assign({}, state, { all });
};

export default (state, action) => {
  switch(action.type) {
    case types.LOAD: return load(state, action);
    case types.LOADED: return loaded(state, action);
    case types.SELECT: return select(state, action);
    case types.INITIALIZE: return initialize(state, action);
    case types.INITIALIZED: return initialized(state, action);
    case types.REMOVE: return remove(state, action);
    case types.REMOVED: return removed(state, action);
    case types.RUN: return wait(state, action);
    case types.RUNNING: return running(state, action);
    case types.STOP: return wait(state, action);
    case types.STOPPED: return stopped(state, action);
    case types.ADD_SETTING: return addSetting(state, action);
    case types.REMOVE_SETTING: return removeSetting(state, action);
    case types.SAVE: return save(state, action);
    case types.SAVED: return saved(state, action);
    default: return state || defaultState;
  }
};
