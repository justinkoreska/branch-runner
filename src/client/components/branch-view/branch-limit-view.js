import React from "react";
import { connect } from "react-redux";
import { selectors as branchSelectors } from "store/branch";
import "./index.scss";

const BranchLimitView = ({
  runningCount,
  branchLimit,
}) => (
  <div className="BranchLimitView">
    <dl>
      <dt>Current number of running artifacts: </dt>
      <dd>{runningCount}</dd>
    </dl>
    <dl>
      <dt>Maximum number of running artifacts: </dt>
      <dd className={runningCount >= branchLimit ? "overlimit" : ""}>{branchLimit}</dd>
    </dl>
    {
      runningCount >= branchLimit
        ? <p className="warning">Please stop unused artifacts in order to run new artifacts.</p>
        : null
    }
  </div>
);

const mapState = state => ({
  runningCount: branchSelectors.runningCount(state),
  branchLimit: branchSelectors.branchLimit(state),
});

export default connect(mapState)(BranchLimitView);
