import express from "express";
import bodyParser from "body-parser";

import routes from "./routes";

const server = express();

server.use(bodyParser.json());

Object.keys(routes).forEach(route => {
  server.use(route, routes[route]);
});

const port = process.env.BRANCHRUNNER_PORT || 5000;

console.log(`Starting server on ${port}`);
server.listen(port);
