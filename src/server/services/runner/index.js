import fs from "fs";
import path from "path";
import { spawn } from "child_process";
import rimraf from "rimraf";
import fetch from "node-fetch";
import unzip from "unzip";

const branchPath = key => `runner/${key}`;
const branchFile = key => `${branchPath(key)}/branch.json`;
const artifactPath = (key, name) => `${branchPath(key)}/${name}.jar`;

class RunnerService {

  _config = {
    killTimeoutSeconds: 1,
    runDelaySeconds: 8,
    javaXms: "64M",
    javaXmx: "64M",
    javaServerPortLow: 55000,
    javaServerPortHigh: 60000,
    basePath: path.resolve(__dirname, "..", ".."),
  }

  constructor(config) {
    this._branch = {};
    this.setConfig(config);
  }

  setConfig(config) {
    if ("object" != typeof config)
      return;
    Object.assign(this._config, config);
  }

  getBranchLimit(serverMemory) {
    const javaXmxConfig = this._config.javaXmx.toUpperCase();
    const javaXmx = javaXmxConfig.includes("G")
      ? javaXmxConfig.replace(/\D/g, "") * 1024
      : javaXmxConfig.replace(/\D/g, "");
    const branchLimit = Math.floor(serverMemory / javaXmx);

    return isNaN(branchLimit) ? 0 : branchLimit;
  }

  resolvePath(p) {
    return path.resolve(this._config.basePath, p);
  }

  initializeBranch(branch) {
    ["", branch.key]
      .map(branchPath)
      .map(p => this.resolvePath(p))
      .forEach(dir =>
        fs.existsSync(dir) || fs.mkdirSync(dir)
      );

    this._branch[branch.key] = branch;
    this.saveBranch(branch);

    return this.fetchAllArtifacts(branch);
  }

  deleteBranch(key) {
    return new Promise(resolve => {
      const branch = this.loadBranch(key);
      branch.artifacts
        .filter(artifact => artifact.running)
        .forEach(artifact => {
          try {
            process.kill(artifact.running.pid);
          } catch (e) {}
        });
      setTimeout(() => {
        const path = this.resolvePath(branchPath(key));
        rimraf(path, {}, console.log);
        resolve();
      }, this._config.killTimeoutSeconds * 1000);
    });
  }

  saveBranch(branch) {
    const path = this.resolvePath(branchPath(branch.key));
    if (!fs.existsSync(path))
      return;
    const file = this.resolvePath(branchFile(branch.key));
    const data = JSON.stringify(branch);
    fs.writeFileSync(file, data);
  }

  loadBranch(key) {
    const file = this.resolvePath(branchFile(key));
    if (!fs.existsSync(file))
      return;
    const data = fs.readFileSync(file, "utf8");
    return JSON.parse(data);
  }

  fetchAllArtifacts(branch) {
    const pending = branch.artifacts
      .map((artifact, n) =>
        this.fetchArtifact(branch.key, artifact)
          .then(() => {
            branch.artifacts[n].fetched = new Date().getTime();
          })
      );
    return Promise.all(pending)
      .then(() => {
        branch.created = new Date().getTime();
        this.saveBranch(branch);
        return branch;
      });
  }

  fetchArtifact(key, artifact) {
    return new Promise((resolve, reject) => {
      const path = this.resolvePath(artifactPath(key, artifact.name));
      const file = fs.createWriteStream(path);
      file.on("finish", () => file.close(() => resolve(path)));
      fetch(artifact.href)
        .then(response =>
          response.body
            .pipe(unzip.Parse())
            .on("entry", entry =>
              "application.jar" == entry.path
                ? entry.pipe(file)
                : entry.autodrain()
            )
        )
        .catch(err => {
          fs.unlink(path);
          reject(err.message);
        });
    });
  }

  runArtifact(key, artifactName) {
    return new Promise(resolve => {
      const branch = this.loadBranch(key);
      const path = this.resolvePath(artifactPath(key, artifactName));
      const port = this.getPort();
      const args = [
        "-Xms" + this._config.javaXms,
        "-Xmx" + this._config.javaXmx,
        "-jar",
        path,
        `--server.port=${port}`,
      ];
      const options = {
        detached: true,
        stdio: ["ignore", "ignore", "pipe"],
        env: {
          ...process.env,
          ...branch.settings,
        },
      };

      const setRunning = running => {
        branch.artifacts = branch.artifacts.map(artifact =>
          artifactName == artifact.name
            ? { ...artifact, running }
            : artifact
        );
        this.saveBranch(branch);
        return branch;
      };

      const logtag = `RunnerService.runArtifact ${key}:${artifactName}`;

      const ps = spawn("java", args, options);
      ps.on("exit", code => {
        console.log(`${logtag} stopped (pid=${ps.pid} code=${code})`)
        setRunning(null);
      });
      ps.stderr.on("data", data =>
        console.log(`${logtag} stderr ${data}`)
      );
      ps.unref();

      console.log(`${logtag} started (${this._config.javaXms}-${this._config.javaXmx} pid=${ps.pid} port=${port} settings=${JSON.stringify(branch.settings)})`)

      setRunning({ pid: ps.pid, port });

      setTimeout(
        () => resolve(branch),
        this._config.runDelaySeconds * 1000
      );
    });
  }

  getPort() {
    const MIN = this._config.javaServerPortLow;
    const MAX = this._config.javaServerPortHigh;
    return MIN + Math.floor(Math.random() * (MAX - MIN));
  }

  stopArtifact(key, artifactName) {
    return new Promise(resolve => {
      const branch = this.loadBranch(key);
      const { running: { pid } } = branch.artifacts.find(x => x.name == artifactName);

      branch.artifacts = branch.artifacts.map(artifact =>
        artifactName == artifact.name
          ? { ...artifact, running: null }
          : artifact
      );
      this.saveBranch(branch);

      try {
        if (pid)
          process.kill(pid);
      } catch (e) {}

      resolve(branch);
    });
  }

}

export default RunnerService;
