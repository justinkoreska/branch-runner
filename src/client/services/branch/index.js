
class BranchService {

  fetchBranches() {
    return fetch("/api/branch")
      .then(response => response.json());
  }

  initializeBranch(key) {
    return fetch(`/api/branch/${key}`, { method: "POST" })
      .then(response => response.json());
  }

  saveBranch(branch) {
    return fetch(`/api/branch/${branch.key}`, {
        method: "PUT",
        body: JSON.stringify(branch),
        headers: {
          "Content-type": "application/json",
        },
      })
      .then(response => response.json());
  }

  runBranch(key, artifactName) {
    return fetch(`/api/branch/${key}/${artifactName}`, { method: "POST" })
      .then(response => response.json());
  }

  stopBranch(key, artifactName) {
    return fetch(`/api/branch/${key}/${artifactName}`, { method: "DELETE" })
      .then(response => response.json());
  }

  removeBranch(key, artifactName) {
    return fetch(`/api/branch/${key}`, { method: "DELETE" })
      .then(response => response.json());
  }

  getRuningBranchLimit() {
    return fetch("/api/branch/limit")
      .then(response => response.json());
  }
}

export default BranchService;
