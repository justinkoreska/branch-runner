import express from "express";

import ConfigService from "../services/config";
import RunnerService from "../services/runner";
import BambooService from "../services/bamboo";

const defaultLocaleBasedOnArtifactName = (name) => {
  switch (name) {
    case "flightnetwork-site":
      return "/en-CA/";
    case "flightnetwork-au-site":
      return "/en-AU/";
    case "flyfar-ca-site":
      return "/en-CA/";
    case "flyfar-apac-site":
      return "/en-AU/";
    default:
      return "";
  }
};

const configService = new ConfigService();

const bambooConfig = {
  urlBase: configService.getVariable("bamboo.urlBase"),
  planKey: configService.getVariable("bamboo.planKey"),
};

if (!bambooConfig.urlBase || !bambooConfig.planKey)
  throw new Error("Missing Bamboo config! Please set BAMBOO_URLBASE and BAMBOO_PLANKEY.");

const bambooService = new BambooService(bambooConfig.urlBase, bambooConfig.planKey);
const runnerService = new RunnerService({ javaXms: "64M", javaXmx: "256M" });

const setDefaultLocaleForAllArtifacts = (branch) => {
  return {
    ...branch,
    artifacts: branch.artifacts.map(
      artifact => ({
        ...artifact,
        defaultLocale: defaultLocaleBasedOnArtifactName(artifact.name)
      })
    )
  }
};

const router = express.Router();

const errorHandler = (response, error) => {
  console.log(error);
  response
    .status(500)
    .send({ error });
};

router.get("/branch", (request, response) => {
  bambooService
    .fetchBranches()
    .then(branches => {
      const branchesWithStatus = branches.map(branch => ({
        ...branch,
        ...runnerService.loadBranch(branch.key),
      }));
      response.send(branchesWithStatus);
    })
    .catch(error => errorHandler(response, error));
});

router.get("/branch/limit", (request, response) => {
  const serverMemory = configService.getVariable("server.memory");

  response.send({branchLimit: runnerService.getBranchLimit(serverMemory)});
});

router.post("/branch/:key", (request, response) => {
  const { key } = request.params;
  const success = branch =>
    response
      .status(201)
      .send(branch);
  const error = message =>
    response
      .status(400)
      .send({ message });
  const initializeBranch = branch =>
    runnerService
      .initializeBranch(setDefaultLocaleForAllArtifacts(branch))
      .then(success);
  bambooService
    .fetchResult(key)
    .then(initializeBranch)
    .catch(error => errorHandler(response, error));
});

router.put("/branch/:key", (request, response) => {
  const { key } = request.params;
  const branchChanges = request.body || {};
  const success = branch =>
    response
      .status(201)
      .send(branch);
  const error = message =>
    response
      .status(400)
      .send({ message });
  const branch = {
    ...runnerService.loadBranch(key),
    ...branchChanges,
  };
  runnerService.saveBranch(branch);
  success(branch);
});

router.delete("/branch/:key", (request, response) => {
  const { key } = request.params;
  runnerService
    .deleteBranch(key)
    .then(() => response.send({}))
    .catch(error => errorHandler(response, error));
});

router.post("/branch/:key/:build", (request, response) => {
  const { key, build } = request.params;
  runnerService
    .runArtifact(key, build)
    .then(branch => {
      response
        .status(200)
        .send(branch);
    })
    .catch(error => errorHandler(response, error));
});

router.delete("/branch/:key/:build", (request, response) => {
  const { key, build } = request.params;
  runnerService
    .stopArtifact(key, build)
    .then(branch => {
      response
        .status(200)
        .send(branch);
    })
    .catch(error => errorHandler(response, error));
});

export { router };
