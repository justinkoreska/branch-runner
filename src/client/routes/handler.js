import React from "react";
import { connect } from "react-redux";

import { actions as configActions } from "store/config";
import {
  actions as branchActions,
  selectors as branchSelectors,
} from "store/branch";

import config from "../config";

class RouteHandler extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.setConfig();
    this.props.loadBranches();
    this.branchRoutes(this.props);
  }

  componentWillReceiveProps(props) {
    this.branchRoutes(props);
  }

  branchRoutes(props) {
    const {
      path,
      selectBranch,
    } = props;

    const branchMatch =
      path.match(/^\/branches\/?([\w\-]*)/);
    if (branchMatch && branchMatch[1])
      selectBranch(branchMatch[1]);
  }

  render() {
    return null;
  }
}

const mapState = state => ({
  path: state.router.location.pathname,
});

const mapDispatch = dispatch => ({
  setConfig: () => dispatch(configActions.setConfig(config)),
  loadBranches: () => dispatch(branchActions.load()),
  selectBranch: key => dispatch(branchActions.select(key)),
});

export default connect(mapState, mapDispatch)(RouteHandler);
