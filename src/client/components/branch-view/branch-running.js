import React from "react";
import { connect } from "react-redux";
import BranchLimitView from "./branch-limit-view";

import {
  actions as branchActions,
  selectors as branchSelectors,
} from "store/branch";

import BranchSettings from "./branch-settings";

const BranchRunningView = ({
  branch,
  runArtifact,
  stopArtifact,
  removeBranch,
  runningCount,
  branchLimit,
}) => (
  <section className="BranchRunningView">
    <h3 className="title is-3">
      {branch.name}
    </h3>
    {branch.build &&
      <h4 className="subtitle is-4">
        <a target="_blank" href={`http://bamboo.flightnetwork.com/browse/${branch.key}-${branch.build}`}>
          Build #{branch.build}
        </a>
        &nbsp;&ndash;&nbsp;
        <a target="_blank" href={`https://bitbucket.org/flightnetwork/flightnetwork-microsites/commits/${branch.commit}`}>
          {branch.commit.substring(0, 8)}
        </a>
        &nbsp;&ndash;&nbsp;
        <span dangerouslySetInnerHTML={{__html:branch.summary}}></span>
      </h4>
    }
    <BranchSettings />
    <BranchLimitView />
    <table className="table">
      <thead>
        <tr>
          <th>Status</th>
          <th>Build</th>
          <th>Size</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      {branch.artifacts.map(artifact =>
        <tr key={artifact.name}>
          <td>
            {artifact.running
              ?
                <button
                  className="button is-small is-outlined is-success"
                  onClick={e => stopArtifact(branch.key, artifact.name)}
                >
                  <span className="icon is-small">
                    <i className="fa fa-pause-circle"></i>
                  </span>
                  <span>
                    running
                  </span>
                </button>
              :
                <button
                  className={`button is-small is-outlined is-danger ${artifact.isWaiting && "is-loading"}`}
                  onClick={runningCount < branchLimit && (e => { e.target.blur(); runArtifact(branch.key, artifact.name); })}
                  disabled={runningCount >= branchLimit}
                  title={runningCount >= branchLimit ? "Please stop unused artifacts in order to run new artifacts." : ""}
                >
                  <span className="icon is-small">
                    <i className="fa fa-play-circle"></i>
                  </span>
                  <span>
                    stopped
                  </span>
                </button>
            }
          </td>
          <td>
            {artifact.name}
          </td>
          <td>
            {Math.floor(artifact.size/1024)} kB
          </td>
          <td>
            {artifact.running &&
              <a href={`//${window ? window.location.hostname : "localhost"}:${artifact.running.port}${artifact.defaultLocale}`} target="_blank">
                <span className="icon">
                  <i className="fa fa-rocket"></i>
                </span>
                <span>
                  launch
                </span>
              </a>
            }
          </td>
        </tr>
      )}
      </tbody>
    </table>
    <div className="has-text-centered">
      <button
        className={`button is-large is-danger ${branch.isRemoving && "is-loading"}`}
        onClick={e => removeBranch(branch.key)}
      >
        Reset Branch
      </button>
    </div>
  </section>
);

const mapState = state => ({
  branch: branchSelectors.selected(state),
  runningCount: branchSelectors.runningCount(state),
  branchLimit: branchSelectors.branchLimit(state),
});

const mapDispatch = dispatch => ({
  runArtifact: (key, artifactName) => dispatch(branchActions.run(key, artifactName)),
  stopArtifact: (key, artifactName) => dispatch(branchActions.stop(key, artifactName)),
  removeBranch: key => dispatch(branchActions.remove(key)),
});

export default connect(mapState, mapDispatch)(BranchRunningView);
