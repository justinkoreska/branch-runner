import React from "react";
import {
  Route,
  Switch,
} from "react-router-dom";

import HomePage from "components/home-page";
import BranchesPage from "components/branches-page";
import ErrorPage from "components/error-page";

import RouteHandler from "./handler";

export default (
  <div>
    <Route component={RouteHandler} />
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/branches" component={BranchesPage} />
      <Route path="/error" component={ErrorPage} />
      <Route component={ErrorPage} />
    </Switch>
  </div>
);
