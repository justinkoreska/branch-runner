
export default {
  LOAD: "BRANCH_LOAD",
  LOADED: "BRANCH_LOADED",
  SELECT: "BRANCH_SELECT",
  INITIALIZE: "BRANCH_INITIALIZE",
  INITIALIZED: "BRANCH_INITIALIZED",
  REMOVE: "BRANCH_REMOVE",
  REMOVED: "BRANCH_REMOVED",
  RUN: "BRANCH_RUN",
  RUNNING: "BRANCH_RUNNING",
  STOP: "BRANCH_STOP",
  STOPPED: "BRANCH_STOPPED",
  ADD_SETTING: "BRANCH_ADD_SETTING",
  REMOVE_SETTING: "BRANCH_REMOVE_SETTING",
  SAVE: "BRANCH_SAVE",
  SAVED: "BRANCH_SAVED",
};
