import React from "react";
import { Link } from "react-router-dom";

import "./index.scss";

const BranchListView = ({
  branches = [],
  loadBranches,
  isLoading,
  selectBranch,
  selected,
  searchTerm,
  filterType,
  onSearch,
  onFilter,
}) => (
  <nav className="BranchList panel">
    <div className="panel-heading">
      Branches
      <button
        className={`button is-pulled-right is-small ${isLoading && "is-loading"}`}
        onClick={e => loadBranches()}
        >
        <i className="fa fa-refresh"></i>
      </button>
    </div>
    <div className="panel-block">
      <p className="control has-icons-left">
        <input className="input is-small" type="text" placeholder="search" value={searchTerm} onChange={e => onSearch(e.target.value)} />
        <span className="icon is-small is-left">
          <i className="fa fa-search"></i>
        </span>
      </p>
    </div>
    <p className="panel-tabs">
      {["all", "running"].map((filter, index) =>
        <a
          key={index}
          className={`${filter == filterType && "is-active"}`}
          onClick={e => onFilter(filter)}
          >
          {filter}
        </a>
      )}
    </p>
    <div className="BranchList-scrollable">
    {branches.map(branch =>
      <Link
        key={branch.key}
        to={`/branches/${branch.key}`}
        className={`panel-block ${selected == branch.key && "is-active"}`}
        >
        {branch.created &&
          <span className="panel-icon is-pulled-right">
            <i className="fa fa-rocket"></i>
          </span>
        }
        <span className="panel-icon">
          <i className="fa fa-code-fork"></i>
        </span>
        {branch.name}
      </Link>
    )}
    </div>
    <div className="panel-block"></div>
  </nav>
);

export default BranchListView;
