import React from "react";
import { connect } from "react-redux";

import {
  actions as branchActions,
  selectors as branchSelectors,
} from "store/branch";

const BranchSettingsView = ({
  branch,
  toggleSettings,
  settingsOpen,
  settingKey,
  settingValue,
  updateKey,
  updateValue,
  addSetting,
  removeSetting,
  saveBranch,
}) => (
  <div>
    <a className="button is-white" onClick={e => toggleSettings()}>
      <span className="icon">
        <i className="fa fa-gear"></i>
      </span>
      <span>
        Branch Settings
      </span>
    </a>
    <div className={`modal ${settingsOpen && "is-active"}`}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">
            Branch Settings
          </p>
          <button
            className="delete"
            aria-label="close"
            onClick={e => toggleSettings(e)}
            >
          </button>
        </header>
        <section className="modal-card-body">

          <table className="table is-fullwidth is-striped">
            <tbody>
              {Object.keys(branch.settings || {}).map(key =>
                <tr key={key}>
                  <td>
                    {key}
                  </td>
                  <td>
                    {branch.settings[key]}
                  </td>
                  <td width="1%">
                    <a href onClick={e => { e.preventDefault(); removeSetting(key); }}>
                      <span className="icon is-small">
                        <i className="fa fa-trash"></i>
                      </span>
                    </a>
                  </td>
                </tr>
              )}
            </tbody>
          </table>

          <form className="form">
            <div className="field is-horizontal">
              <div className="field-body">
                <div className="field">
                  <p className="control is-expanded">
                    <input
                      type="text"
                      className="input"
                      value={settingKey}
                      onChange={e => updateKey(e.target.value)}
                    />
                  </p>
                </div>
                <div className="field">
                  <p className="control is-expanded">
                    <input
                      type="text"
                      className="input"
                      value={settingValue}
                      onChange={e => updateValue(e.target.value)}
                    />
                  </p>
                </div>
                <div className="field">
                  <p className="control is-expanded">
                    <button
                      className="button"
                      onClick={e => { e.preventDefault(); addSetting(); }}
                      >
                      <span className="icon">
                        <i className="fa fa-plus"></i>
                      </span>
                    </button>
                  </p>
                </div>
              </div>
            </div>
          </form>

        </section>
        <footer className="modal-card-foot">
          <button
            className="button is-success"
            onClick={e => saveBranch()}
            >
            Save
          </button>
          <button
            className="button"
            onClick={e => toggleSettings()}
            >
            Cancel
          </button>
        </footer>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={e => toggleSettings()}
        >
      </button>
    </div>
  </div>
);

class BranchSettingsController extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      settingsOpen: false,
      settingKey: "",
      settingValue: "",
    };
  }

  toggleSettings() {
    this.setState({
      settingsOpen: !this.state.settingsOpen,
    });
  }

  updateKey(key) {
    this.setState({
      settingKey: key,
    });
  }

  updateValue(value) {
    this.setState({
      settingValue: value,
    });
  }

  addSetting() {
    const {
      settingKey: key,
      settingValue: value,
    } = this.state;
    if (!key || !value) return;
    this.props.addSetting(this.props.branch.key, key, value);
    this.setState({
      settingKey: "",
      settingValue: "",
    });
  }

  removeSetting(key) {
    this.props.removeSetting(this.props.branch.key, key);
  }

  saveBranch() {
    this.props.saveBranch(this.props.branch);
    this.toggleSettings();
  }

  render() {
    return <BranchSettingsView {...{
      ...this.props,
      ...this.state,
      toggleSettings: this.toggleSettings.bind(this),
      updateKey: this.updateKey.bind(this),
      updateValue: this.updateValue.bind(this),
      addSetting: this.addSetting.bind(this),
      removeSetting: this.removeSetting.bind(this),
      saveBranch: this.saveBranch.bind(this),
    }} />;
  }
}

const mapState = state => ({
  branch: branchSelectors.selected(state),
});

const mapDispatch = dispatch => ({
  addSetting: (key, settingKey, settingValue) =>
    dispatch(branchActions.addSetting(key, settingKey, settingValue)),
  removeSetting: (key, settingKey) =>
    dispatch(branchActions.removeSetting(key, settingKey)),
  saveBranch: branch => dispatch(branchActions.save(branch)),
});

export default connect(mapState, mapDispatch)(BranchSettingsController);
